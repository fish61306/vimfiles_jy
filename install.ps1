# download pathogen
curl -LSso pathogen.vim https://tpo.pe/pathogen.vim
mkdir ~/vimfiles/autoload -Force
mv pathogen.vim ~/vimfiles/autoload/pathogen.vim

# update submodules
git submodule update --init --recursive

# copy vimrc to home path
cp _vimrc ~/ -Force
cp vimfiles/ ~/ -Recurse -Force

# copy build.py for visual studio 2022
cp .\tmp_modifications\youcompleteme\build.py  ~\vimfiles\bundle\YouCompleteMe\third_party\ycmd\build.py

# copy PythonSupport.h to avoid ssize_t missing error
cp ./tmp_modifications/youcompleteme/PythonSupport.h ~\vimfiles\bundle\YouCompleteMe\third_party\ycmd\cpp\ycm\PythonSupport.h 

$current_path = get-location
cd ~/vimfiles/bundle/YouCompleteMe
python install.py --clangd-completer --msvc 17
cd $current_path
