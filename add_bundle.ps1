
$plugin_urls = @(
  'https://github.com/fholgado/minibufexpl.vim.git',
  'https://github.com/tomasr/molokai.git',
  'https://github.com/preservim/nerdcommenter.git',
  'https://github.com/preservim/nerdtree.git',
  'https://github.com/preservim/tagbar.git',
  'https://github.com/SirVer/ultisnips.git',
  'https://github.com/sheerun/vim-polyglot.git',
  'https://github.com/tpope/vim-sensible.git',
  'https://github.com/honza/vim-snippets.git',
  'https://github.com/milkypostman/vim-togglelist.git'
  'https://github.com/ycm-core/YouCompleteMe.git'
)

foreach( $plugin_url in $plugin_urls){
  $basename = split-path $plugin_url -leafBase
  git submodule add $plugin_url "vimfiles/bundle/$basename"
}
