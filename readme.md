## Test Environment

* Windows 10
* PowerShell 7.1.15
* Python 3.10
* Visual Studio 2022 Preview
* CMake 3.22.0-rc2
* Git 2.33.1.windows.1
