let mapleader =","
" ---------------------------------- "
" Pathogen
" ---------------------------------- "

" Load plugins with pathogen
execute pathogen#infect()
execute pathogen#helptags()

" ---------------------------------- "
" Configure NERDTree
" ---------------------------------- "

" Open NERDTree when Vim startsup and no files were specified
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif

" Open NERDTree with Ctrl-n 
map <F5> :NERDTreeToggle<CR>

" Close Vim if the only window left open is NERDTree
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTreeType") && b:NERDTreeType == "primary") | q | endif

" ---------------------------------- "
" Configure NERDcommenter
" ---------------------------------- "
" Add spaces after comment delimiters by default
let g:NERDSpaceDelims = 1

" Use compact syntax for prettified multi-line comments
let g:NERDCompactSexyComs = 1

" Align line-wise comment delimiters flush left instead of following code indentation
let g:NERDDefaultAlign = 'left'

" Set a language to use its alternate delimiters by default
let g:NERDAltDelims_java = 1

" Add your own custom formats or override the defaults
let g:NERDCustomDelimiters = { 'c': { 'left': '/**','right': '*/' } ,
    \ 'verilog': { 'left': '//', 'leftAlt': '/*', 'rightAlt': '*/' },
    \ 'py':{ 'left': '#','leftAlt': '#'},
    \ 'vim':{ 'left': '"'}}

" Allow commenting and inverting empty lines (useful when commenting a region)
let g:NERDCommentEmptyLines = 1

" Enable trimming of trailing whitespace when uncommenting
let g:NERDTrimTrailingWhitespace = 1

" ---------------------------------- "
" Configure MiniBufExpl
" ---------------------------------- "

" Open MiniBufExpl with Ctrl-m
map <F4> :MBEToggle<CR>

noremap <leader><tab> :MBEbp<CR> 
noremap <leader><s-tab> :MBEbn<CR> 
" ---------------------------------- "
" Configure Tagbar
" ---------------------------------- "

" Open Tagbar with F2
map <F2> :TagbarToggle<CR>

" ---------------------------------- "
" Configure Ultisnip and YouCompleteMe
" ---------------------------------- "

let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<tab>"
let g:UltiSnipsJumpBackwardTrigger="<s-tab>"

" ---------------------------------- "
" Configure YouCompleteMe
" ---------------------------------- "


let g:ycm_key_list_select_completion = ['<C-n>']
let g:ycm_key_lmst_previous_completion = ['<C-p>']
let g:ycm_confirm_extra_conf = 0
let g:ycm_confirm_extra_conf = 0
let g:ycm_key_list_select_completion=[]
let g:ycm_key_list_previous_completion=[]
set completeopt-=preview

" Goto definition with F3
map <F3> :YcmCompleter GoTo<CR>
map <F9> :YcmCompleter FixIt<CR>

" ---------------------------------- "
" Configure Togglelist
" ---------------------------------- "

nnoremap <script> <silent> <leader>l :call ToggleLocationList()<CR>
nnoremap <script> <silent> <leader>q :call ToggleQuickfixList()<CR>

" ---------------------------------- "
" Basic function
" ---------------------------------- "

" ---------------------------------- "
" Common
" ---------------------------------- "

" Python shiftwidth, tabstop, softtabstop
autocmd FileType python set sw=4
autocmd FileType python set ts=4
autocmd FileType python set sts=4

autocmd bufnewfile *.{cpp,h,hpp} so ~/vimfiles/cpp_header/cHeader.txt
autocmd bufnewfile *.{cpp,h,hpp} exe "1," . 10 . "g/File Name :.*/s//File Name : " .expand("%")
autocmd bufnewfile *.{cpp,h,hpp} exe "1," . 10 . "g/Creation Date :.*/s//Creation Date : " .strftime("%Y %b %d %X")
autocmd Bufwritepre,filewritepre   *.{cpp,h,hpp} execute "normal ma"
autocmd Bufwritepre,filewritepre   *.{cpp,h,hpp} exe "1," . 10 . "g/Last Modified :.*/s/Last Modified :.*/Last Modified : ".strftime("%Y %b %d %X")
autocmd bufwritepost,filewritepost *.{cpp,h,hpp} execute "normal `a"
autocmd bufnewfile *.{cpp,h,hpp} exe "normal Go" 
function! s:insert_gates()
  let gatename = substitute(toupper(expand("%:t")), "\\.", "_", "g")
  execute "normal! i#ifndef " . gatename
  execute "normal! o#define " . gatename . " "
  execute "normal! Go#endif /* " . gatename . " */"
  normal! kk
endfunction
autocmd BufNewFile *.{h,hpp} call <SID>insert_gates()
"=============colorscheme========================
let g:molokai_original = 1

colorscheme molokai


"=============HotKey=============================
inoremap <c-h> <Left>
inoremap <c-j> <Down>
inoremap <c-k> <Up>
inoremap <c-l> <Right>
inoremap <c-a> <esc>


"==============Basic Setting======================

syntax on

set encoding=utf-8

set nu

set relativenumber

set tabstop=2

set scrolloff=3

set expandtab

"set autoindent 

"C/C++
set cindent 

set smartindent

"set indent width = 2
set shiftwidth=2

set showmatch

set hlsearch

set showcmd

:inoremap ( ()<ESC>i
:inoremap { {}<ESC>i
:inoremap [ []<ESC>i

"==============xterm(putty)=============="

